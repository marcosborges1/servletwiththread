package com.crs4gae;

public abstract class CRSFile {
	public abstract String getPath();
	public abstract boolean exists();
	public abstract String getAbsolutePath();
	public abstract boolean mkdirs();
	public abstract CRSFile[] listFiles();
	public abstract CRSFile[] listFiles(CRSFilenameFilter filter);
	public abstract boolean isDirectory();
	public abstract String getName();
	public abstract boolean delete();
}
