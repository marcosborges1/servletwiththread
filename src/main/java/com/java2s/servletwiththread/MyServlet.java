package com.java2s.servletwiththread;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.crs4gae.CRS4GAEThread;
import com.crs4gae.CRSThread;

public class MyServlet extends HttpServlet implements Runnable {

	long lastprime = 0;
	Date lastprimeModified = new Date();
	CRSThread searcher;

	public void init() throws ServletException {
		searcher = new CRS4GAEThread(this);
		searcher.start();
	}

	public void run() {
	    long candidate = 2;

	    while (true) {     
	      candidate += 2;                    
	      try {
	        searcher.sleep(5000);
	      }
	      catch (InterruptedException ignored) { }
	      lastprime = 1;   
	    }
	  }

	  public void doGet(HttpServletRequest req, HttpServletResponse res)
	                               throws ServletException, IOException {
	    res.setContentType("text/plain");
	    PrintWriter out = res.getWriter();
	    if (lastprime == 0) {
	      out.println("Still searching for first prime...");
	    }
	    else {
	      out.println("The last prime discovered was " + lastprime);
	      out.println(" at " + lastprimeModified);
	    }
	  }

	  public void destroy() {
	    searcher.stop();
	  }
}
