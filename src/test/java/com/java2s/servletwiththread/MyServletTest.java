package com.java2s.servletwiththread;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyServletTest {

	private FirefoxDriver driver;
	ServletPage servletPage;

	@Before
	public void initialize() {
		this.driver = new FirefoxDriver();
		this.servletPage = new ServletPage(driver,true);
	}

	@Test
	public void checkRunningThreadInital() {
		servletPage.visit();
		assertTrue(servletPage.existsContent("Still searching for first prime..."));
	}
	
	@Test
	public void checkRunningThreadAfterRefresh() {
		servletPage.visit();
		assertTrue(servletPage.existsContent("The last prime discovered was 1"));
	}

	@After
	public void close() {
		driver.close();
	}

}
