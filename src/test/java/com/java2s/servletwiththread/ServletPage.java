package com.java2s.servletwiththread;

import org.openqa.selenium.WebDriver;

public class ServletPage {
	
	private WebDriver driver;
	private boolean isLocal;
	
	public ServletPage(WebDriver driver, boolean isLocal) {
		this.driver = driver;
		this.isLocal = isLocal;
	}
	
	public void visit() {	
		String url = (isLocal) ? "http://localhost:8080/":"http://servletwiththread.appspot.com";
		driver.get(url);
	}
	
	public boolean existsContent(String str) {
		return driver.getPageSource().contains(str);
	}
}
